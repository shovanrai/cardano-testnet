# Cardano Testnet Development

Testnen Explorer       https://explorer.cardano-testnet.iohkdev.io/ \
Mainnet Explorer       https://explorer.cardano.org/en \
IOHK Alonzo Exercises  https://github.com/input-output-hk/Alonzo-testnet/tree/main/Alonzo-solutions \
Cardano Developer Docs https://developers.cardano.org/docs/get-started/ 

# Setup
- ### Add directory structure

- Download the cardano-node binary from IOHK (or build yourself) :version 1.29.0 \
```
mkdir cardano
cd cardano
tar -xf cardano-node-1.29.0-linux.tar.gz
mv cardano* ~/.local/bin/
```
- ### Download the configuration files 
 
Main-net 
```
wget https://hydra.iohk.io/build/7370192/download/1/mainnet-config.json
wget https://hydra.iohk.io/build/7370192/download/1/mainnet-byron-genesis.json
wget https://hydra.iohk.io/build/7370192/download/1/mainnet-shelley-genesis.json
wget https://hydra.iohk.io/build/7370192/download/1/mainnet-alonzo-genesis.json
wget https://hydra.iohk.io/build/7370192/download/1/mainnet-topology.json
```
Test Net 
```
wget https://hydra.iohk.io/job/Cardano/cardano-node/cardano-deployment/latest-finished/download/1/testnet-config.json
wget https://hydra.iohk.io/job/Cardano/cardano-node/cardano-deployment/latest-finished/download/1/testnet-byron-genesis.json
wget https://hydra.iohk.io/job/Cardano/cardano-node/cardano-deployment/latest-finished/download/1/testnet-shelley-genesis.json
wget https://hydra.iohk.io/job/Cardano/cardano-node/cardano-deployment/latest-finished/download/1/testnet-alonzo-genesis.json
wget https://hydra.iohk.io/job/Cardano/cardano-node/cardano-deployment/latest-finished/download/1/testnet-topology.json
```

- Export Path
```
echo export CARDANO_NODE_SOCKET_PATH=~/cardano/db/node.socket >> .bashrc
source .bashrc
```
- Run mainnet 
```
cardano-node run \
--topology mainnet-files/mainnet-topology.json \
--database-path mainnet/db \
--socket-path mainnet/db/node.socket \
--host-addr 0.0.0.0 \
--port 3001 \
--config mainnet-files/mainnet-config.json
```
- changed mainnet/db/protocolMagicId to 764824073 if you have testnet-magic Number

- Run testnet
 ```
 cardano-node run \
--topology testnet-files/testnet-topology.json \
--database-path db \
--socket-path db/node.socket \
--host-addr 0.0.0.0 \ 
--port 3001 \
--config testnet-files/testnet-config.json
```
Sometimes absolute path worked and sometimes relative path
```
cardano-node run --topology $HOME/cardadno/testnet-files/testnet-topology.json --database-path $HOME/cardano/db/ --socket-path $HOME/cardano/db/node.socket --host-addr 0.0.0.0 --port 3001 --config $HOME/testnet-files/testnet-config.json
```
- Check Node
```
cardano-cli query tip --testnet-magic 1097911063
```

## Wallets Addresses and Transactions 
- Generate payment key-pair 
```
mkdir /home/shovanrai/cardano/keys
```
```
cardano-cli address key-gen \
--verification-key-file $HOME/cardano/keys/payment1.vkey \
--signing-key-file $HOME/cardano/keys/payment1.skey
```
.vkey -> Public Verification Key, Derive Cardano Wallet address \
.skey -> Private Signing     Key, Sign/Approve a Transaction

- Generate Wallet Address
```
cardano-cli address build \
--payment-verification-key-file $HOME/cardano/keys/payment1.vkey \
--out-file /$HOME/cardano/keys/payment1.addr \
--testnet-magic 1097911063
```
- Note
mainnet addresses are prefixed with the string value addr1. \
testnet addresses are prefixed with the string value addr_test1. 

- Check utxos in wallet address
```
cardano-cli query utxo \
--testnet-magic 1097911063 \
--address $(cat $HOME/cardano/keys/payment1.addr)
```
- Get tAda from https://developers.cardano.org/docs/integrate-cardano/testnet-faucet/ \
You get a TxHash which can be used to view complete transaction on the Cardano Blockchain Explorer

- Create another wallet
```
cardano-cli address key-gen \
--verification-key-file $HOME/cardano/keys/payment2.vkey \
--signing-key-file $HOME/cardano/keys/payment2.skey
```
```
cardano-cli address build \
--payment-verification-key-file keys/payment2.vkey \
--out-file keys/payment2.addr \
--testnet-magic 1097911063
```
- Check Utxo at that address
```
cardano-cli query utxo --$testnet --address $addres
```
## Sending Funds 

- Query Protocol Parameters
```
cardano-cli query protocol-parameters \
--testnet-magic 1097911063 \
--out-file json-files/protocol.json
```

- Create a draft transactioni to calculate fee required for real transaction
```
cardano-cli transaction build-raw \
--tx-in 36a76a37cc8071f0d5ae10085613b306d8c6b4ef0507bb6e635e3fe5cd5d3f7d#0 \
--tx-out $(cat keys/payment2.addr)+0 \
--tx-out $(cat keys/payment1.addr)+0 \
--fee 0 \
--out-file draft/min-fee.draft  
```

- Calculate min fee
```
cardano-cli transaction calculate-min-fee \
--tx-body-file draft/min-fee.draft \
--tx-in-count 1 \
--tx-out-count 2 \
--witness-count 1 \
--testnet-magic 1097911063 \
--protocol-params-file json-files/protocol.json
```
This gives an amount 174169 Lovelace

- Real Transaction with actual fee
```
cardano-cli transaction build-raw \
--tx-in 36a76a37cc8071f0d5ae10085613b306d8c6b4ef0507bb6e635e3fe5cd5d3f7d#0 \
--tx-out $(cat keys/payment2.addr)+250000000 \
--tx-out $(cat keys/payment1.addr)+749825831 \ # 1000000000 - 250000000 -fee
--fee 174169 \
--out-file draft/tx.draft
```
- Signing the Transaction
```
cardano-cli transaction sign \
--tx-body-file cardano/draft/tx.draft \
--signing-key-file keys/payment1.skey \
--testnet-magic 1097911063 \
--out-file cardano/signed/tx.signed
```

- Submiting Tthe Transaction
```
cardano-cli transaction submit \
--tx-file signed/tx.signed \
--testnet-magic 1097911063
```
- Note from the Cardano Team \
_If you have waited too long to sign and submit the transaction, the fees might've changed \
during that time and therefore the transaction might get rejected by the network. To solve this, \
you simply have to recalculate the fees, rebuild the transaction, sign it and submit it!_

## Create a Wallet with cardano-wallet
- Download Binary from https://hydra.iohk.io/build/7788057 \
(This version had cardano-node 1.30.0 with different ghc version but it worked) \
- copy cardano-address and cardano-wallet to ```~/.local/bin/```
- Enable port TCP 1337
```
mkdir -p /home/shovanrai/cardano/wallets
```
```
cardano-wallet serve \
--port 1337 \
--testnet testnet-files/testnet-byron-genesis.json \
--database wallets/db \
--node-socket $CARDANO_NODE_SOCKET_PATH
```
```
curl --request GET \
--url http://localhost:1337/v2/network/information | jq
```
- Check your browser at http://localhost:1337/v2/network/information

- Create 24-word mnemonic seed
```
cardano-wallet recovery-phrase generate 
```
- Create Cardano wallet using /v2/wallets API endpoints
```
curl --request POST --url http://localhost:1337/v2/wallets --header 'Content-Type: application/json' --data '{
"name": "test_cf_1",
"mnemonic_sentence" : ["alone", "minor", "shallow", "trip", "one", "cycle", "glow", "hammer", "reject", "canoe", "enter", "keep", "void", "crouch", "switch", "crop", "stand", "paddle", "refuse","purity", "render", "strike", "camp", "throw"],"passphrase": "test123456" }' | jq
```
wallet id ```3e7025f1c23b81de0bbb20caeed94b3362c92dfe```
```
curl --request GET --url http://localhost:1337/v2/wallets/3e7025f1c23b81de0bbb20caeed94b3362c92dfe | jq '.state'
```
- Get cardano address
```
curl --request GET \
  --url 'http://localhost:1337/v2/wallets/3e7025f1c23b81de0bbb20caeed94b3362c92dfe/addresses?state=unused' | jq '.[0]'
```
```
curl --request GET \
  --url http://localhost:1337/v2/wallets/5076b34c6949dbd150eb9c39039037543946bdce | jq '.balance'
```
- Another wallet
```
 curl --request POST --url http://localhost:1337/v2/wallets --header 'Content-Type: application/json' --data '{
"name": "test_cf_2",
"mnemonic_sentence" : ["cotton", "apple", "cradle", "bargain", "omit", "guess", "truly", "fat", "scrap", "vanish", "clutch", "weather", "gospel", "weather", "same", "beef", "marriage", "cinnamon", "auction", "educate","stand", "vital", "dry", "shield"],"passphrase": "test123456" }' | jq
```
``` 
curl --request GET \
  --url 'http://localhost:1337/v2/wallets/e8334195dca1eb25d5d9fb2be5be55954e309a81/addresses?state=unused' | jq '.[0]'
```
- Send funds
```
curl --request POST \
  --url http://localhost:1337/v2/wallets/3e7025f1c23b81de0bbb20caeed94b3362c92dfe/transactions \
  --header 'Content-Type: application/json' \
  --data '{
    "passphrase": "test123456",
    "payments": [
        {
            "address": "addr_test1qrgux8ppyz8pe4ep2jz7jjgqm6ly8nvlccjrzgltfmpg2taz40ltky5fk888fnmn422un0m7px9pcjke7jkfppjq6dnqfw76mk",
            "amount": {
                "quantity": 100000000,
                "unit": "lovelace"
            }
        }
    ]
}'
```
- Check Balance
```
curl --request GET \
  --url http://localhost:1337/v2/wallets/e8334195dca1eb25d5d9fb2be5be55954e309a81| jq '.balance'
```
API https://input-output-hk.github.io/cardano-wallet/api/edge

### Multi Witness Transaction 
Read https://developers.cardano.org/docs/integrate-cardano/multi-witness-transactions-cli
### Receiving Payments (cardano-wallet/cardano-cli) \
Read https://developers.cardano.org/docs/integrate-cardano/listening-for-payments-cli
### Transaction Metadata 
https://developers.cardano.org/docs/transaction-metadata/
Four Major Types: \
    Validation and verification \
    Authentication and attribution \
    Secure record of information \
    Timestamping

- Check utxo ```cardano-cli query utxo --testnet-magic 1097911063 --address $(cat keys/payment1.addr)```
TxHash ```250d4befc50f9613cad3061240ccaa0c1f5747a8b2f0f3105041e48b35b10300#1```

- Draft transaction for min fee calculation
```
cardano-cli transaction build-raw \
--tx-in 250d4befc50f9613cad3061240ccaa0c1f5747a8b2f0f3105041e48b35b10300#1 \
--tx-out $(cat keys/payment1.addr)+0 \
--metadata-json-file json-files/metadata1.json \
--fee 0 \
--out-file draft/fee-metadata1.draft
```
- Fee Calculation
```
cardano-cli transaction calculate-min-fee \
--tx-body-file draft/fee-metadata1.draft \
--tx-in-count 1 \
--tx-out-count 1 \
--witness-count 1 \
--byron-witness-count 0 \
--testnet-magic 1097911063 \
--protocol-params-file json-files/protocol.json
```
```172893 Lovelace ``` 

- Real Transaction
```
cardano-cli transaction build-raw \
--tx-in 250d4befc50f9613cad3061240ccaa0c1f5747a8b2f0f3105041e48b35b10300#1 \
--tx-out $(cat keys/payment1.addr)+399304600 \
--metadata-json-file json-files/metadata1.json \
--fee 172893 \
--out-file draft/tx-metadata1.draft
```

- Sign transaction
```
cardano-cli transaction sign \
--tx-body-file draft/tx-metadata1.draft \
--signing-key-file keys/payment1.skey \
--testnet-magic 1097911063 \
--out-file signed/tx-metadata1.signed
```

- Submit 
```
cardano-cli transaction submit \
--tx-file signed/tx-metadata1.signed \
--testnet-magic 1097911063
```
TxHash with metadata ```42963025087e9a6e832e70f15af5d87c5711ba1535d1c62348e74d3b6aba46d9#0```

### Metadata cardano-wallet 
- Get address wallet 2 
```
curl --request GET --url 'http://localhost:1337/v2/wallets/e8334195dca1eb25d5d9fb2be5be55954e309a81/addresses?state=unused' | jq '.[0]["id"]'
```
Transaction @ cardano/wallet-transaction-POST-copy
- Saved log for future reference
```
echo '''{"status":"pending","withdrawals":[],"amount":{"quantity":171969,"unit":"lovelace"},"inputs":[{"amount":{"quantity":100000000,"unit":"lovelace"},"address":"addr_test1qrgux8ppyz8pe4ep2jz7jjgqm6ly8nvlccjrzgltfmpg2taz40ltky5fk888fnmn422un0m7px9pcjke7jkfppjq6dnqfw76mk","id":"c4f2c040c33fce83b3b80c7f244efecc077104bf835153567ce4022049d79a7b","assets":[],"index":0}],"direction":"outgoing","fee":{"quantity":171969,"unit":"lovelace"},"outputs":[{"amount":{"quantity":1000000,"unit":"lovelace"},"address":"addr_test1qz96aur3dn246y62f3zsaszwdt45tam7ph9n43tguwqfufaz40ltky5fk888fnmn422un0m7px9pcjke7jkfppjq6dnqra4xnt","assets":[]},{"amount":{"quantity":98828031,"unit":"lovelace"},"address":"addr_test1qppskl7w9xwapra2k8msjaudssg8r5vtqqxlydh3mkkwpfaz40ltky5fk888fnmn422un0m7px9pcjke7jkfppjq6dnqesqlje","assets":[]}],"script_validity":"valid","expires_at":{"time":"2021-09-30T06:41:23Z","epoch_number":159,"absolute_slot_number":38614867,"slot_number":296467},"pending_since":{"height":{"quantity":2953306,"unit":"block"},"time":"2021-09-30T04:41:16Z","epoch_number":159,"absolute_slot_number":38607660,"slot_number":289260},"metadata":{"1337":{"map":[{"k":{"string":"name"},"v":{"string":"hello world"}},{"k":{"string":"completed"},"v":{"int":0}}]}},"id":"30714d9acaa6f8c9b5374ae583d2dccd49a0b8fdd6fb6366d1e72863322723f5","deposit":{"quantity":0,"unit":"lovelace"},"collateral":[],"mint":[]}''' | jq
```

- Retreiving metadata
```
curl -H 'project_id: <api_key>' https://cardano-mainnet.blockfrost.io/api/v0/metadata/txs/labels/1337 | jq
```
- cardano-db-sync
- cardano-graphql
- cardano-wallet
- Omigos


### Native Assets and NFTS 
Create a new wallet and use cardano/native-assets dir for everything related to NFTS and N-A \
- Set vaiables in shell
```
testnet="testnet-magic 1097911063"
tokenname1="Testtoken"
tokenname2="SecondTesttoken"
tokenamount="10000000"
output="0"
```
```cd native-assets``` \
```cardano-cli address key-gen --verification-key-file keys/payment.vkey --signing-key-file keys/payment.skey``` \
```cardano-cli address build --payment-verification-key-file keys/payment.vkey --out-file keys/payment.addr --$testnet``` \
```address=$(cat keys/payment.addr)``` \
```cardano-cli query protocol-parameters --$testnet --out-file protocol.json```
```
- Policy keys
cardano-cli address key-gen \
--verification-key-file policy/policy.vkey \
--signing-key-file policy/policy.skey
```
- Create policy.script \
```touch policy/policy.script && echo "" > policy/policy.script```
```
echo "{" >> policy/policy.script
echo "  \"keyHash\": \"$(cardano-cli address key-hash --payment-verification-key-file policy/policy.vkey)\"," >> policy/policy.script
echo "  \"type\": \"sig\"" >> policy/policy.script
echo "}" >> policy/policy.script
```

- Generate key-hash -> cardano-cli address key-hash --payment-verification-key-file policy/policy.vkey
- Generate policy ID
```cardano-cli transaction policyid --script-file ./policy/policy.script >> policy/policyID``` \
```cardano-cli query utxo --address $address --$testnet``` \
```Output dab9e898dbc81b9c8115a20b05a6888fea84340e19d06892d2ad2ac902fb36cc     0        1000000000 lovelace + TxOutDatumHashNone``` 

```
txhash="dab9e898dbc81b9c8115a20b05a6888fea84340e19d06892d2ad2ac902fb36cc"
txix="0"
funds="1000000000"
policyid=$(cat policy/policyID)
```
` calculate fee with $fee 3000000
```
cardano-cli transaction build-raw \
 --fee $fee \
 --tx-in $txhash#$txix \
 --tx-out $address+$output+"$tokenamount $policyid.$tokenname1 + $tokenamount $policyid.$tokenname2" \
 --mint="$tokenamount $policyid.$tokenname1 + $tokenamount $policyid.$tokenname2" \
 --minting-script-file policy/policy.script \
 --out-file draft/matx.raw
 ```

 ```
 fee=$(cardano-cli transaction calculate-min-fee \
 --tx-body-file draft/matx.raw \
 --tx-in-count 1 --tx-out-count 1 \
 --witness-count 1 \
 --$testnet \
 --protocol-params-file protocol.json | cut -d " " -f1)
```
```new $fee is 177601``` \
``` output=$(expr $funds - $fee)```

- Real Transaction
```
cardano-cli transaction build-raw \
--fee $fee  \
--tx-in $txhash#$txix  \
--tx-out $address+$output+"$tokenamount $policyid.$tokenname1 + $tokenamount $policyid.$tokenname2" \
--mint="$tokenamount $policyid.$tokenname1 + $tokenamount $policyid.$tokenname2" \
--minting-script-file policy/policy.script \
--out-file draft/real-matx.raw
```
- Sign Transaction
```
cardano-cli transaction sign  \
--signing-key-file keys/payment.skey  \
--signing-key-file policy/policy.skey  \
--$testnet --tx-body-file draft/real-matx.raw  \
--out-file signed/matx.signed
```

- Submit Transaction
```cardano-cli transaction submit --tx-file signed/matx.signed --$testnet```

- SEND TOKEN TO WALLET 

txhash of utxo with native token \
receiver_output = 1 Ada minimum must be sent for a valid Transaction, Here we send 10 to be safe
reciever address = cardano/keys/payment1.addr
- Set variables
```
fee="0"
receiver="addr_test1vpty86rn8dvrs2rmzkm5da68nfusx9dcjgxv58wcyr74rcgjm3nw0"
receiver_output="10000000"
txhash="ac67480cab4aae9d678726f00edcaa038933c13ebd81151ffa7937e489461dac"
txix="0"
funds="999822399"
```
- Raw Transaction sending 2 $tokenname1
```
cardano-cli transaction build-raw  \
--fee $fee  \
--tx-in $txhash#$txix  \
--tx-out $receiver+$receiver_output+"2 $policyid.$tokenname1"  \
--tx-out $address+$output+"9999998 $policyid.$tokenname1 + 10000000 $policyid.$tokenname2"  \
--out-file draft/rec_matx.raw
```
- Actual fee calculation from draft

```
fee=$(cardano-cli transaction calculate-min-fee --tx-body-file rec_matx.raw --tx-in-count 1 --tx-out-count 2 --witness-count 1 --$testnet --protocol-params-file protocol.json | cut -d " " -f1)
```
new fee is ```179493```

- Actual output
```
output=$(expr $funds - $fee - 10000000)
```
new output is ```989642906```

- Real Transaction
```
cardano-cli transaction build-raw  \
--fee $fee  \
--tx-in $txhash#$txix  \
--tx-out $receiver+$receiver_output+"2 $policyid.$tokenname1"  \
--tx-out $address+$output+"9999998 $policyid.$tokenname1 + 10000000 $policyid.$tokenname2"  \
--out-file draft/real-rec_matx.raw
```

- Sign and Submit Transaction
```
cardano-cli transaction sign --signing-key-file keys/payment.skey --$testnet --tx-body-file draft/real-rec_matx.raw --out-file signed/real-rec_matx.signed
cardano-cli transaction submit --tx-file signed/rec_matx.signed --$testnet
```
- Burning Tokens
```
cardano-cli query utxo --address $address --$testnet
                           TxHash                                 TxIx        Amount
--------------------------------------------------------------------------------------
0eee1c49bb515581783343e2e5778b6fd02dbe2d764c36c95e18e090e79c4956     1        989642906 lovelace + 10000000 08c10536bc4977c01837ec65b2a82e0ada64c2649f1d6b2575e4c519.SecondTesttoken + 9999998 08c10536bc4977c01837ec65b2a82e0ada64c2649f1d6b2575e4c519.Testtoken + TxOutDatumHashNone
```
- Variables
```
txhash="insert your txhash here" 0eee1c49bb515581783343e2e5778b6fd02dbe2d764c36c95e18e090e79c4956
txix="insert your TxIx here" 1
funds="insert Amount only in here"989642906
burnfee="0"
policyid=$(cat policy/policyID)
burnoutput="0"
```

- Fee Calculation
```
cardano-cli transaction build-raw \
--fee $burnfee \
--tx-in $txhash#$txix \
--tx-out $address+$burnoutput+"9999998 $policyid.$tokenname1 + 9995000 $policyid.$tokenname2"  \
--mint="-5000 $policyid.$tokenname2" \
--minting-script-file policy/policy.script \
--out-file draft/burning.raw
```
- Fee
```
burnfee=$(cardano-cli transaction calculate-min-fee --tx-body-file draft/burning.raw --tx-in-count 1 --tx-out-count 1 --witness-count 1 --$testnet --protocol-params-file protocol.json | cut -d " " -f1)
```
```176677```
- Output
```
burnoutput=$(expr $funds - $burnfee)
```
```989466229```

- Real Transaction
```
cardano-cli transaction build-raw \
 --fee $burnfee \
 --tx-in $txhash#$txix \
 --tx-out $address+$burnoutput+"9999998 $policyid.$tokenname1 + 9995000 $policyid.$tokenname2"  \
 --mint="-5000 $policyid.$tokenname2" \
 --minting-script-file policy/policy.script \
 --out-file draft/real-burning.raw
```

- Sign Transaction
```
cardano-cli transaction sign  \
--signing-key-file keys/payment.skey  \
--signing-key-file policy/policy.skey  \
--$testnet  \
--tx-body-file draft/real-burning.raw  \
--out-file signed/burning.signed
```

- Submit Transaction
```cardano-cli transaction submit --tx-file burning.signed --$testnet```
```
cardano-cli query utxo --address $address --$testnet
                           TxHash                                 TxIx        Amount
--------------------------------------------------------------------------------------
f00eb0ae850dd147af445de659092a1bcee5c814060c04bd4ee53a87340742a6     0        989466229 lovelace + 9995000 08c10536bc4977c01837ec65b2a82e0ada64c2649f1d6b2575e4c519.SecondTesttoken + 9999998 08c10536bc4977c01837ec65b2a82e0ada64c2649f1d6b2575e4c519.Testtoken + TxOutDatumHashNone
```
Send tokens to cardano-wallet ```e8334195dca1eb25d5d9fb2be5be55954e309a81``` \
address at localhost:1337/wallets/walled_id/addresses?state=unused \
```addr_test1qz3quww6wnhayzdj0rxeu9fduxp7ysqyslvuessf80yfzn9z40ltky5fk888fnmn422un0m7px9pcjke7jkfppjq6dnqzcy3wu```

### MINTING NFT testnet 
policyID     -> Unique Identifier -> Generated from policyscript -> defines minter and actions \
Metadata     -> See ./metadata-example \
Time locking -> So no-one can mint after deadline has passed
```
mkdir nft 
cd nft
```
- Set variables 
```
tokenname="NFT1"
tokenamount="1"
fee="0"
output="0"
ipfs_hash="hash of the ipfs file"
```
ipfs hash for eastern-sunset ```QmemtfqYpiwwgMr9T6sNxdsTKCxnKMYCTQZCVTm1Nu81bV```
- New address generation / not required if address already present
```
cardano-cli address key-gen --verification-key-file keys/paymentnft.vkey --signing-key-file keys/paymentnft.skey
cardano-cli address build --payment-verification-key-file keys/paymentnft.vkey --out-file keys/paymentnft.addr --testnet-magic 1097911063
address=$(cat keys.paymentnft.addr)
```
```745fbed729b5a25d515e736a1a9d584af38e51dfbc59b7ec1888f554563be252``` TxHash after cardano-cli query utxo testnet address

```cardano-cli query protocol-parameters --testnet-magic 1097911063 --out-file protocol.json```

- Policy
```
cardano-cli address key-gen \
--verification-key-file policy/policy.vkey \
--signing-key-file policy/policy.skey
```
Make policy.script -> see policy/policy-maker-command \
Make KeyHash ```cardano-cli address key-hash --payment-verification-key-file policy/policy.vkey```

policyID
```cardano-cli transaction policyid --script-file ./policy/policy.script >> policy/policyID```

Metadata --> see metadata/testnet-NFT1-metadata

```cardano-cli query utxo --address $address --testnet-magic 1097911063 ```
TxHash ```745fbed729b5a25d515e736a1a9d584af38e51dfbc59b7ec1888f554563be252```

- Setting variables
```
txhash=745fbed729b5a25d515e736a1a9d584af38e51dfbc59b7ec1888f554563be252
txix="0"
funds="10000000"
policyID=$(cat policy/policyID)
slotnumber=from-policy.script
script="policy/policy.script"
```
- Draft for fee calculation
```
cardano-cli transaction build-raw \
--fee $fee --tx-in $txhash#$txix \
--tx-out $address+$output+"$tokenamount $policyID.$tokenname" \
--mint="$tokenamount $policyID.$tokenname" \
--minting-script-file $script \
--metadata-json-file metadata/testnet-NFT1-metadata.json  \
--invalid-hereafter $slotnumber \
--out-file draft/testnet-NFT1.raw
```
- Fee and Output calculation
```
fee=$(cardano-cli transaction calculate-min-fee --tx-body-file draft/testnet-NFT1.raw --tx-in-count 1 --tx-out-count 1 --witness-count 1 --mainnet --protocol-params-file protocol.json | cut -d " " -f1)
```
```output=$(expr $funds - $fee)```

- Real Transaction with new fee and output
```
cardano-cli transaction build-raw \
--fee $fee  \
--tx-in $txhash#$txix  \
--tx-out $address+$output+"$tokenamount $policyID.$tokenname" \
--mint="$tokenamount $policy.$tokenname" \
--minting-script-file $script \
--metadata-json-file metadata/testnet-NFT1-metadata.json \
--invalid-hereafter $slotnumber \
--out-file draft/real-testnet-NFT1.raw
```
- Sign Transaction
```
cardano-cli transaction sign  \
--signing-key-file keys/paymentnft.skey  \
--signing-key-file policy/policy.skey  \
--mainnet --tx-body-file draft/real-testnet-NFT.raw  \
--out-file signed/testnet-NFT1.signed
```
```
cardano-cli transaction submit --tx-file signed/testnet-NFT1.signed --testnet-magic 1097911063
cardano-cli query utxo --address $address --testnet-magic 10979911063
```
### Burning Token
- Variables
```
burnfee="0"
burnoutput="0"
txhash="c41d13131674e22b065e9232c2d539320fc254e99789ad4ad0974638c1f162f2"
txix="0"
```
- Draft
```
cardano-cli transaction build-raw --fee $burnfee --tx-in $txhash#$txix --tx-out $address+$burnoutput --mint="-1 $policyID.$tokenname" --minting-script-file $script --invalid-hereafter $slot --out-file burning.raw
```
- Fee and Output calculation
```
burnfee=$(cardano-cli transaction calculate-min-fee --tx-body-file burning.raw --tx-in-count 1 --tx-out-count 1 --witness-count 1 --testnet-magic 1097911063 --protocol-params-file protocol.json | cut -d " " -f1)
burnoutput=$(expr $funds - $burnfee)
```
- Real Transaction
```
cardano-cli transaction build-raw --fee $burnfee --tx-in $txhash#$txix --tx-out $address+$burnoutput --mint="-1 $policyiID.$tokenname" --minting-script-file 
$script --invalid-hereafter 38839200 --out-file burning.raw
```
- Sign Transaction
```
cardano-cli transaction sign  --signing-key-file keys/paymentnft.skey  --signing-key-file policy/policy.skey --testnet-magic 1097911063  --tx-body-file real-burning.raw --out-file burning.signed
```
- Check
```
cardano-cli transaction submit --tx-file burning.signed --testnet-magic 1097911063
```
