# Alonzo testnet Exercise 3
## Plutus scripts

_***Notes:***_
- We used build instead of build-raw which makes it easier since the fee is automatically calculated.
- Use correct ordering while building transaction (cardano-cli transaction build --help)

_***Exercise***_ https://github.com/input-output-hk/Alonzo-testnet/blob/main/Alonzo-exercises/alonzo-purple/3_Alonzo-purple-exercise-3.md \
_***Solution***_ https://github.com/input-output-hk/Alonzo-testnet/blob/main/Alonzo-solutions/exercise3/e3SampleSolution.md

### Create a new wallet address 

```
cardano-cli address key-gen --verification-key-file keys/wallet.vkey --signing-key-file keys/wallet.skey
```

```
cardano-cli address build --payment-verification-key keys/wallet.vkey --stake-verification-key keys/stake.vkey --testnet-magic 1097911063
```

```
cardano-cli address build --payment-verification-key-file keys/wallet.vkey --stake-verification-key-file keys/stake.vkey --out-file keys/wallet.addr --testnet-magic 1097911063
```

```
cardano-cli query utxo --testnet-magic 1097911063 --address $(cat keys/payment1.addr)
```

```
cardano-cli transaction build --alonzo-era --tx-in $txhash#0 --tx-out $(cat keys/wallet.addr)+100000000 --change-address $(cat keys/payment1.addr) --testnet-magic 1097911063 --out-file plutus-testnet/tx1.raw
```

```
cardano-cli transaction sign --testnet-magic 1097911063 --signing-key-file keys/payment1.skey --tx-body-file plutus-testnet/tx1.raw --out-file plutus-testnet/tx1.sign
```

```
cardano-cli transaction submit --testnet-magic 1097911063 --tx-file plutus-testnet/tx1.sign
```

### Lock Funds in a Script

- Generate a random number and save it to random_datum.txt

- Generate hash 
```
cardano-cli transaction hash-script-data --script-data-value $(cat random_datum.txt) >> random_datum_hash.txt
```

- Generate the script address 
```
cardano-cli address build --payment-script-file AlwaysSucceeds.plutus --testnet-magic 1097911063 --out-file script.addr
```

- Generate Protocol Parameters 
```
cardano-cli query protocol-parameters --testnet-magic 1097911063 > pparams.json
```

- Build a transaction (fee is shown as std-out) 
```
cardano-cli transaction build \
--alonzo-era \
--tx-in $txhash#1 \
--tx-out $(cat script.addr)+20000000 \
--tx-out-datum-hash $(cat random_datum_hash.txt) \
--change-address $(cat wallet.addr) \
--testnet-magic 1097911063 \
--protocol-params-file pparams.json \
--out-file tx2.raw
```

```
cardano-cli transaction sign --tx-body-file tx2.raw --signing-key-file wallet.skey --testnet-magic 1097911063 --out-file tx2.sign
```

```
cardano-cli transaction submit --testnet-magic 1097911063 --tx-file tx2.sign
```

```
cardano-cli query utxo --testnet-magic 1097911063 --address $(cat script.addr)
```

_--tx-in-collateral -> collateral in case calidation fails, usually 100% or more of fee_

- Find the txhas corrosponding to random_number_hash
```
cardano-cli query utxo --testnet-magic 1097911063 --address $(cat script.addr) | grep 1d24e34bf0f3d0d19fa0c6c3541f44ff950ca3357c7437ed14f1d51ede2ab73b
```

```
cardano-cli transaction build \
--alonzo-era \
--tx-in 6e40f70f476b836f04cb24c6c04ee5cfa19c25e1f1c62dcea40d142294f0073b#1 \
--tx-in-script-file AlwaysSucceeds.plutus \
--tx-in-datum-value $(cat random_datum.txt) \
--tx-in-redeemer-value $(cat random_datum.txt) \
--tx-in-collateral b2029ae95d97a47f871983f2d670360e2151c21525e6c5deb5924be276178d56#1 \
--change-address $(cat wallet.addr) \
--protocol-params-file pparams.json \
--testnet-magic 1097911063 \
--out-file tx_spend_from_script.raw
```
_tx-in is the script transaction, tx-in-collateral is the collateral transaction (wallet)_

```
cardano-cli transaction sign --tx-body-file tx_spend_from_script.raw --signing-key-file wallet.skey --testnet-magic 1097911063 --out-file tx_spend_from_script.sign
```

```
cardano-cli transaction submit --testnet-magic 1097911063 --tx-file tx_spend_from_script.sign
```

```
cardano-cli query utxo --testnet-magic 1097911063 --address $(cat wallet.addr)
```
# Alonzo Testnet Exercise 4

_***Exercise***_ https://github.com/input-output-hk/Alonzo-testnet/blob/main/Alonzo-exercises/alonzo-purple/4_Alonzo-purple-exercise-4.md
_***Solution***_ https://github.com/input-output-hk/Alonzo-testnet/blob/main/Alonzo-solutions/exercise4/Exercise4-solution.md

### Compiling Plutus Script
- Clone Alonzo-Testnet repo
```
git clone https://github.com/input-output-hk/Alonzo-testnet.git
```

- Installing Requirements 
```
cd Alonzo-testnet/resources/plutus-sources/plutus-helloworld
```
_***Note:*** Do the same for Alwayssucceeds and other scripts._
```
nix-shell -p ghc cabal-install libsodium zlib pkg-config systemd
```
_***Note:*** Use default.nix or nix flakes to manage dependencies._

- Create a cabal.project.local file (Got ```udefined symbol: crypto_vrf_proofbytes``` ERROR without it)
```
echo "package cardano-crypto-praos" >> cabal.project.local
echo "flags: -external-libsodium-vrf" >> cabal.project.local
```
*** Objectives from Exercise 4 ***
1. Compile the AlwaysSucceeds Plutus script from source and extract the serialised representation that the compiler has produced.
2. Compile the HelloWorld Plutus script from source. Save the serialised Plutus script into a file helloworld.plutus.
```
cabal build -w ghc-8.10.4
```
```
cat plutus-helloworld.cabal | grep executable
```
```
cabal run plutus-helloworld -- 1 helloworld2.plutus
```
3. Build a Cardano transaction that will submit helloworld.plutus for execution on the testnet. As before, you will need to provide two input addresses: one to pay for the transaction fees and one to provide the collateral that is spent if the Plutus script fails to validate (to avoid risking all your funds, it is recommended that you use a dedicated payment address with limited funds to pay for the collateral rather than your main payment address!).

```
cd ~/cardano/plutus-testnet
```
```
cardano-cli address build --payment-script-file Alonzo-testnet/resources/plutus-sources/plutus-helloworld/helloworld2.plutus --testnet-magic 1097911063 --out-file helloworld2.add
```

