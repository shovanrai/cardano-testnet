# All things cadano related

Plutus-Apps https://github.com/input-output-hk/plutus-apps

Plutus-Pioneer-Program https://github.com/input-output-hk/plutus-pioneer-program

Alonzo Testnet Exercises: https://github.com/input-output-hk/Alonzo-testnet

Testnen Explorer       https://explorer.cardano-testnet.iohkdev.io/ \

Mainnet Explorer       https://explorer.cardano.org/en \

IOHK Alonzo Exercises  https://github.com/input-output-hk/Alonzo-testnet/tree/main/Alonzo-solutions \

Cardano Developer Docs https://developers.cardano.org/docs/get-started/ 

## Plutus Pioneer

``` 
git clone https://github.com/input-output-hk/plutus-apps.git 
```

```
git clone https://github.com/input-output-hk/plutus-pioneer-program.git 
```
- Checkout the correct branch from cabal.project
- Add the IOHK binary cache in configuration.nix

- ``` cd projects/plutus-apps ``` and then run ``` nix-shell ```

- ``` cd projects/plutus-pioneer-program ``` and then ``` cabal build ``` and ``` cabal repl ```

- ``` cd plutus-apps/plutus-playground-server ``` and run ``` plutus-playground-server``` while in nix-shell
- In another nix-shell run ``` npm start ``` while in the same directory.
- Serve haddocs``` build-and-serve-docs ``` 

